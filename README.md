# podman

Running OCI-based containers. https://podman.io/

* [*Troubleshooting: A list of common issues and solutions for Podman*
  ](https://github.com/containers/podman/blob/main/troubleshooting.md)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=docker+podman+buildah+skopeo&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Semi-official documentation
* [*What happens behind the scenes of a rootless Podman container?*
  ](https://www.redhat.com/sysadmin/behind-scenes-podman)
  2020-02 Matthew Heon (Red Hat), Dan Walsh (Red Hat), Giuseppe Scrivano (Red Hat)
* [*How to run podman from inside a container*
  ](https://stackoverflow.com/questions/56032747/how-to-run-podman-from-inside-a-container)
  (2019) Stack Overflow

# Cloud computing
* [*Launching Containers using Podman and Libpod*
  ](https://www.katacoda.com/courses/containers-without-docker/running-containers-with-podman)
  @ Katacoda
